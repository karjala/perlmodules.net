use v5.34;
use warnings;
use Test2::V0;

use Local::Util 'pg_dt', 'es_dt';

my $time = 1635144449;
my $pg_dt = pg_dt($time);
is $pg_dt, '2021-10-25 06:47:29+0000', 'epoch to pg';
my $dt1 = pg_dt($pg_dt);
is $dt1->stringify, '2021-10-25T06:47:29', 'pg to DateTime';
is pg_dt($dt1), $pg_dt, 'DateTime to pg';

my $es_dt = es_dt($time);
is $es_dt, '2021-10-25T06:47:29Z', 'epoch to ES';
my $dt2 = es_dt($es_dt);
cmp_ok $dt2, '==', $dt1, 'ES to DateTime';
is es_dt($dt2), $es_dt, 'DateTime to ES';

done_testing;
