#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';
use lib "$RealBin/../../local/lib/perl5", "$RealBin/../../lib";

use PLib::Init;
use Local::Util 'p';

use Mojo::Pg;
# use Parallel::ForkManager;
use List::AllUtils 'uniq';

my $pg = Mojo::Pg->new('postgresql://dbuser:dbuser@localhost/pmnet');
my $db = $pg->db;

say 'reading modules...';
my $existing_modules_by_name = $db->select(
    'modules',
    [qw/ id name distro_id /],
)->hashify(['name']);

say 'reading rels...';
my @rel_ids = $db->select(
    'releases',
    'id',
    undef,
    { order_by => { -desc => 'datetime' } },
)->hashes->map(sub {$_->{id}})->@*;

say scalar @rel_ids;

my $count = 0;

RELEASE:
foreach my $rel_id (@rel_ids) {
    if (++$count % 10 == 0) {
        say "Doing the ${count}'th release, of ", scalar(@rel_ids);
    }
    # find distro_id
    my $distro_id = $db->select(
        'releases',
        'distro_id',
        { id => $rel_id },
    )->array->[0] or die;

    # find modules provided
    my $provides_names = $db->query(
        q|SELECT "data"->'provides' FROM "release_es" WHERE rel_id = ?|,
        $rel_id,
    )->expand->arrays->[0][0];

    NEW_MODULE_NAME:
    foreach my $new_name (uniq @$provides_names) {
        # say $new_name;
        if (! exists $existing_modules_by_name->{$new_name}) {
            my $mod_id = $db->insert(
                'modules',
                {
                    name      => $new_name,
                    distro_id => $distro_id,
                },
                { returning => 'id' }
            )->hashes->[0]{id};

            $existing_modules_by_name->{$new_name} = {
                id        => $mod_id,
                name      => $new_name,
                distro_id => $distro_id,
            };
        } else {
            if (defined $existing_modules_by_name->{$new_name}{distro_id}) {
                next NEW_MODULE_NAME;
            } else {
                $db->update(
                    'modules',
                    { distro_id => $distro_id },
                    { id => $existing_modules_by_name->{$new_name}{id} },
                );

                $existing_modules_by_name->{$new_name}{distro_id} = $distro_id;
            }
        }
    }
}
