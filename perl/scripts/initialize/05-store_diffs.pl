#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';
use lib "$RealBin/../../local/lib/perl5", "$RealBin/../../lib";

use PLib::Diff;
use Local::Util 'pg_dt';

use Mojo::Pg;
use Parallel::ForkManager;
use List::AllUtils 'sort_by', 'max_by';
use version 'is_lax';

use constant NUM_PROCESSES => 16;

use experimental 'builtin';
use builtin 'indexed';

sub is_valid_version ($version_str) {
    my $produced_warning = 0;
    local $SIG{__WARN__} = sub ($msg) {
        $produced_warning = 1;
        warn $msg;
    };
    return is_lax($version_str) && eval { version->parse($version_str); 1 } && ! $produced_warning;
}

my $pg = Mojo::Pg->new('postgresql://dbuser:dbuser@localhost/pmnet');
my $db_g = $pg->db;

# setting is_processed to !content
say 'setting is_processed to !content';
$db_g->update(
    'changes',
    { is_processed => 1 },
    {
        content      => undef,
        is_processed => 0,
    },
);
say 'set';

# setting is_processed to invalid versions
say 'setting is_processed to invalid versions';
my @rels_w_invalid_versions = $db_g->select(
    'releases',
    [qw/ id version /],
)->hashes->@*;
@rels_w_invalid_versions = grep { ! is_valid_version($_->{version}) } @rels_w_invalid_versions;
say "\tfound them";
$db_g->update(
    'changes',
    { is_processed => 1 },
    {
        rel_id       => {-in, [map $_->{id}, @rels_w_invalid_versions]},
        is_processed => 0,
    },
);
say "\tset them";

# find distros
my @distro_ids = $db_g->select(
    [
        'distros',
        ['releases', distro_id => 'distros.id'],
        ['changes', rel_id => 'releases.id'],
    ],
    'distros.id',
    { "changes.is_processed" => 0 },
    {
        order_by => 'distros.id',
        group_by => ['distros.id'],
    },
)->hashes->map(sub {$_->{id}})->@*;
say 'scalar(@distro_ids) = ', scalar(@distro_ids);

# set up pm
my $pm = Parallel::ForkManager->new(NUM_PROCESSES);
$pm->set_waitpid_blocking_sleep(0.1);
my $num_finished = 0;
$pm->run_on_finish(sub {
    my ($pid, $exit_code, $ident) = @_;

    say "Distro $ident had an error: exit_code = $exit_code" if $exit_code;

    $num_finished++;
    say "Finished $num_finished distros" if $num_finished % 10 == 0;
});

DISTRO:
while (my $distro_id = shift @distro_ids) {
    $pm->start($distro_id) and next DISTRO;

    my $db = $pg->db;

    # get all distro's releases
    my @rels = $db->select(
        [
            'releases',
            ['changes', rel_id => 'releases.id'],
        ],
        [
            (map "releases.$_", qw/ id version datetime /),
            (map "changes.$_", qw/ content is_processed /),
        ],
        {
            distro_id => $distro_id,
            content   => { '!=', undef },
        },
        { order_by => ['datetime'] },
    )->hashes->@*;

    # grep releases for valid versions and sort them by date
    @rels = grep { is_valid_version($_->{version}) } @rels;

    # turn version strings into version objects
    $_->{version} = version->parse($_->{version}) foreach @rels;

    RELEASE:
    foreach my ($i, $rel) (indexed @rels) {
        ! $rel->{is_processed} or next RELEASE;
        my $prev_rel = do {
            my @smaller = grep {$_->{version} < $rel->{version}} @rels[0 .. $i-1];
            max_by {$_->{version}} reverse @smaller; # largest of small
        };
        # prev_rel might be undef
        my $prev_id = $prev_rel ? $prev_rel->{id} : undef;
        my $prev_content = $prev_rel ? $prev_rel->{content} : '';
        $db->update(
            'changes',
            {
                prev_id      => $prev_id,
                diff         => { -json => PLib::Diff->diff($prev_content, $rel->{content}) },
                is_processed => 1,
            },
            { rel_id => $rel->{id} },
        );
    }

    $pm->finish;
}
