#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';
use lib "$RealBin/../../local/lib/perl5", "$RealBin/../../lib";

use Local::MetacpanAPI;
use Local::Util 'p';

use Mojo::JSON 'to_json';
use Path::Tiny 'path';
use DateTime;
use Parallel::ForkManager;

my $cursor_dt = DateTime->new(
    year      => '1995',
    month     => 7,
    day       => 1,
    hour      => 0,
    minute    => 0,
    second    => 0,
    time_zone => 'UTC',
);

my $data_dir_path = path(__FILE__)->parent(4)->child('data', 'releases');
$data_dir_path->mkpath;

my $pm = Parallel::ForkManager->new(20);
$pm->set_waitpid_blocking_sleep(0.1);

while ($cursor_dt <= DateTime->now(time_zone => 'UTC')) {
    my $from_dt = $cursor_dt->clone;
    my $till_dt = $from_dt->clone->add(months => 6, days => 15);
    $till_dt <= DateTime->now(time_zone => 'UTC')->subtract(months => 1) or next;
    my $filename = "$from_dt-until-$till_dt.json";
    my $file_path = $data_dir_path->child($filename);
    ! -e $file_path->stringify or next;

    $pm->start and next;

    say DateTime->now(time_zone => 'Europe/Athens'), ": Doing from $from_dt until $till_dt";
    
    my @releases = Local::MetacpanAPI->fetch_recent_releases($from_dt, $till_dt)->@*;

    my $json = to_json \@releases;
    $file_path->spew_utf8($json) if @releases;

    $pm->finish;
} continue {
    $cursor_dt->add(months => 6);
}

$pm->wait_all_children;
