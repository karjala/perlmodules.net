#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';
use lib "$RealBin/../../local/lib/perl5", "$RealBin/../../lib";

use Local::Util 'ua';

use Mojo::Pg;
use Mojo::UserAgent;
use Parallel::ForkManager;
use Syntax::Keyword::Try;

use experimental 'signatures';

use constant NUM_PROCESSES => 16;

my $pg = Mojo::Pg->new('postgresql://dbuser:dbuser@localhost/pmnet');
my $db_g = $pg->db;

# set up user agent
my $ua = ua;

my @rels = $db_g->select(
    [
        'releases',
        [-left => 'changes', rel_id => 'id'],
    ],
    ['releases.id', 'releases.author', 'releases.name'],
    { 'changes.rel_id' => undef },
    { sort_by => 'releases.id' },
)->hashes->@*;

say 'scalar(@rels) = ', scalar @rels;

my $pm = Parallel::ForkManager->new(NUM_PROCESSES);
$pm->set_waitpid_blocking_sleep(0.1);
my $num_finished = 0;
$pm->run_on_finish(sub {
    my ($pid, $exit_code, $ident, undef, undef, $data_structure_reference) = @_;

    if (! $exit_code and defined $data_structure_reference) {
        my ($id, $path, $content) = $data_structure_reference->@{qw/ id path content /};
        $db_g->insert(
            'changes',
            {
                rel_id  => $id,
                path    => $path,
                content => $content,
            },
        );
    }

    $num_finished++;
    say "Finished $num_finished releases" if $num_finished % 100 == 0;

    say "Release $ident had an error: exit_code = $exit_code" if $exit_code;
});

RELEASE:
while (my $rel = shift @rels) {
    $pm->start($rel->{id}) and next RELEASE;

    my $id = $rel->{id} or die;
    my $author = $rel->{author} or die;
    my $name = $rel->{name} or die;

    my $url = "https://fastapi.metacpan.org/v1/changes/$author/$name";
    my ($path, $content);
    my $success;
    ATTEMPT:
    for (1..3) {
        try {
            my $res = $ua->get($url)->result;
            if ($res->is_success) {
                ($content, $path) = $res->json->@{qw/ content path /};
                defined $content or die 'no content in success';
                defined $path or die 'no path in success';
            } elsif ($res->code == 404) {
                $content = undef;
                $path = undef;
            } else {
                die 'neither success nor 404';
            }

            $success = 1;

            last ATTEMPT;
        } catch ($e) {
            $success = 0;
            next ATTEMPT;
        };
    }

    if ($success) {
        $pm->finish(0, {
            id      => $id,
            path    => $path,
            content => $content,
        });
    } else {
        $pm->finish;
    }
}

$pm->wait_all_children;
