#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';
use lib "$RealBin/../../local/lib/perl5", "$RealBin/../../lib";

use Local::Util 'pg_dt', 'p';

use Mojo::Pg;
use Mojo::JSON 'from_json', 'to_json';
use Parallel::ForkManager;
use Path::Tiny 'path';
use DateTime::Format::ISO8601;
use List::AllUtils 'sort_by';
use Syntax::Keyword::Try;
use Data::Dump 'dump';
use Encode;
use Carp::Always;

my $pg = Mojo::Pg->new('postgresql://dbuser:dbuser@localhost/pmnet');

my @file_paths = sort(path(__FILE__)->parent(4)->child('data', 'releases')->children);

my $pm = Parallel::ForkManager->new(16);
$pm->set_waitpid_blocking_sleep(0.1);
$pm->run_on_finish(sub {
    my ($pid, $exit_code, $ident) = @_;

    say "$ident had an error: exit_code = $exit_code" if $exit_code;
});
my %map = (
    '\u0000' => '\\\\u0000',
    '\\\\'   => '\\\\',
);
PATH:
foreach my $file_path (@file_paths) {
    $pm->start("$file_path") and next PATH;

    my $db = $pg->db;

    say scalar(localtime), ' : ', $file_path;
    my $file_contents = $file_path->slurp_utf8;
    $file_contents =~ s'(\\\\|\\u0000)'$map{$1}'ge; # pg can't store chr(0) in text/json fields of postgresql
    my @rels = (from_json $file_contents)->@*;
    my %rel_dts = map {(
        $_,
        do {
            my $dt = DateTime::Format::ISO8601->parse_datetime($_->{date});
            $dt->set_time_zone('UTC') if $dt->time_zone->name eq 'floating';
            $dt;
        }
    )} @rels;
    @rels = sort_by { $rel_dts{$_} } @rels;

    RELEASE:
    foreach my $rel (@rels) {
        my $author = $rel->{author};
        my $distro_name = $rel->{distribution};
        my $version = $rel->{version};
        my $name = $rel->{name};
        my $dt = $rel_dts{$rel};

        try {
            $author or die;
            $distro_name or die;
            $version // die;
            $name or die;
            $dt or die;
            $version eq $rel->{metadata}{version} or die;
        } catch ($e) {
            p $rel;
            warn $e;

            my $buggy_exists = !!$db->select(
                'buggy_es',
                undef,
                {
                    author   => $author,
                    rel_name => $name,
                    data     => to_json($rel),
                },
            )->hash;

            $buggy_exists or $db->insert(
                'buggy_es',
                {
                    author   => $author,
                    rel_name => $name,
                    data     => to_json($rel),
                },
            );

            next RELEASE;
        };

        # select or insert distro
        my $tx = $db->begin;
        my $distro_row = $db->select(
            'distros',
            'id',
            { name => $distro_name },
        )->hash || $db->insert(
            'distros',
            { name => $distro_name },
            {
                on_conflict => [ name => { name => \'distros.name' } ],
                returning   => 'id',
            },
        )->hash;
        my $distro_id = $distro_row->{id};

        # select or insert release
        my $rel_row = $db->select(
            'releases',
            ['id', 'datetime', 'distro_id'],
            {
                distro_id => $distro_id,
                author    => $author,
                name      => $name,
                datetime  => pg_dt($dt),
            },
        )->hash || $db->insert(
            'releases',
            {
                distro_id => $distro_id,
                version   => $version,
                datetime  => pg_dt($dt),
                name      => $name,
                author    => $author,
            },
            {
                on_conflict => [['author', 'name'] => {name => \'releases.name'}],
                returning   => ['id', 'datetime', 'distro_id'],
            },
        )->hash;
        pg_dt($rel_row->{datetime}) == $dt
            and $rel_row->{distro_id} == $distro_id
            or die "different datetime or distro_id: $rel_row->{datetime} $dt $rel_row->{distro_id} $distro_id";
        my $rel_id = $rel_row->{id};

        # insert release_es
        $db->select(
            'release_es',
            'rel_id',
            { rel_id => $rel_id },
        )->hash or $db->insert(
            'release_es',
            {
                rel_id => $rel_id,
                data   => to_json($rel),
            },
            { on_conflict => undef },
        );

        $tx->commit;
    }

    $pm->finish;
}

$pm->wait_all_children;
