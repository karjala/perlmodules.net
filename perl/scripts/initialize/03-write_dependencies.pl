#!/usr/bin/env perl

use v5.34;
use warnings;
use FindBin '$RealBin';
use lib "$RealBin/../../local/lib/perl5", "$RealBin/../../lib";

use Local::Util 'p', 'belongs_to';

use Mojo::Pg;
use Mojo::JSON 'from_json';
use Parallel::ForkManager;
use List::AllUtils 'sort_by';

my $pg = Mojo::Pg->new('postgresql://dbuser:dbuser@localhost/pmnet');
my $db = $pg->db;

my @distro_ids = $db->select(
    'distros',
    'id',
)->hashes->map(sub {$_->{id}})->@*;

my $pm = Parallel::ForkManager->new(16);
$pm->set_waitpid_blocking_sleep(0.1);
my $num_finished = 0;
$pm->run_on_finish(sub {
    my ($pid, $exit_code, $ident) = @_;

    $num_finished++;
    say "Finished $num_finished distros" if $num_finished % 100 == 0;

    say "Distro $ident had an error: exit_code = $exit_code" if $exit_code;
});

DISTRO:
foreach my $distro_id (@distro_ids) {
    $pm->start($distro_id) and next DISTRO;

    $db = $pg->db;
    my $tx = $db->begin;

    my ($deps, $rel_id) = $db->select(
        [
            'releases',
            ['release_es', rel_id => 'id'],
        ],
        [ \"release_es.data->'dependency' AS deps", \'releases.id AS id' ],
        { distro_id => $distro_id },
        {
            order_by => [ { -desc => 'datetime' }, { -desc => 'id' } ],
            limit    => 1,
        },
    )->hash->@{qw/ deps id /};
    $deps = from_json $deps;

    my @output;
    foreach my $dep (sort_by {"$_->{module} $_->{phase} $_->{relationship}"} @$deps) {
        my $mod_name = $dep->{module} or die 'missing module';
        my $phase = $dep->{phase} or die 'missing phase';
        my $relationship = $dep->{relationship} or die 'missing relationship';

        my $mod_row = $db->select(
            'modules',
            'id',
            { name => $mod_name },
        )->hash;
        my $mod_id = $mod_row ? $mod_row->{id} : $db->insert(
            'modules',
            { name => $mod_name },
            {
                on_conflict => ['name' => {name => \'modules.name'}],
                returning => 'id',
            },
        )->hash->{id};

        push @output, "\t$rel_id\t$mod_name\t$phase\t$relationship";
        eval {
            $db->insert(
                'deps',
                {
                    distro_id    => $distro_id,
                    module_id    => $mod_id,
                    phase        => $phase,
                    relationship => $relationship,
                },
                { on_conflict => undef },
            );
        };
        if (my $e = $@) {
            say foreach sort @output;
            die $e;
        }
    }

    $tx->commit;

    $pm->finish;
}
