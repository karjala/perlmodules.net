#!/usr/bin/env perl

use v5.36;
use warnings;
use FindBin '$RealBin';
use lib "$RealBin/../../local/lib/perl5", "$RealBin/../../lib";

use PmNet;
use PLib::Init;
use PLib::Diff;
use Local::MetacpanAPI;
use Local::Util qw/ p pg_dt iso_dt ua eqq /;

use Mojo::Pg;
use Mojo::JSON 'to_json', 'from_json';
use DateTime;
use Syntax::Keyword::Try;
use List::AllUtils 'max_by', 'uniq', 'sort_by';
use version 'is_lax';

no autovivification;

use experimental 'builtin', 'for_list';
use builtin 'indexed';

my $app = PmNet->new;

my $pg = Mojo::Pg->new('postgresql://dbuser:dbuser@localhost/pmnet');

while (1) {
    my $db = $pg->db;

    # Reading existing modules
    say 'reading modules...';
    my $existing_modules_by_name = $db->select(
        'modules',
        [qw/ id name distro_id /],
    )->hashify(['name']);

    my $last_row = $db->select(
        'releases',
        'datetime',
        undef,
        {
            order_by => { -desc, 'datetime' },
            limit    => 1,
        },
    )->hash;
    my $last_dt = $last_row ? pg_dt($last_row->{datetime}) : pg_dt('1980-01-01 00:00:00+0000');

    p $last_dt;

    my @rels = Local::MetacpanAPI->fetch_recent_releases($last_dt->clone->subtract(weeks => 2))->@*;
    p scalar(@rels) . ' releases';

    # map releases to dt's, and then sort
    my %rel_dts = map {(
        $_,
        iso_dt $_->{date},
    )} @rels;
    @rels = sort_by { $rel_dts{$_} } @rels;

    my $count_all = @rels;

    RELEASE:
    foreach my ($count, $rel) (indexed @rels) {
        say "doing release $count of $count_all...";

        # 1. start transaction
        my $tx = $db->begin;

        # 2. sanitize, validate or store in buggy_es and next
        my $dt = $rel_dts{$rel};

        # sanitize
        my $json = to_json($rel);
        my %map = (
            '\u0000' => '\\\\u0000',
            '\\\\'   => '\\\\',
        );
        $json =~ s'(\\\\|\\u0000)'$map{$1}'ge; # pg can't store chr(0) in text/json fields of postgresql
        $rel = from_json $json;

        # validate
        my $author = $rel->{author};
        my $distro_name = $rel->{distribution};
        my $version = $rel->{version};
        my $name = $rel->{name};

        try {
            $author or die;
            $distro_name or die;
            $version // die;
            $name or die;
            $dt or die;
            $version eq $rel->{metadata}{version} or die;
        } catch ($e) {
            p $rel;
            warn $e;

            $db->insert(
                'buggy_es',
                {
                    author   => $author,
                    rel_name => $name,
                    data     => to_json($rel),
                },
            ) unless $db->exists(
                'buggy_es',
                {
                    author   => $author,
                    rel_name => $name,
                    data     => to_json($rel),
                },
            );

            $tx->commit;

            next RELEASE;
        };

        # 4. select or insert distro
        my $distro_row = $db->select(
            'distros',
            'id',
            { name => $distro_name },
            { for => 'update' },
        )->hash || $db->insert(
            'distros',
            { name => $distro_name },
            {
                on_conflict => [ name => { name => \'distros.name' } ],
                returning   => 'id',
            },
        )->hash;
        my $distro_id = $distro_row->{id};

        # 5. insert release, skipping if already exists
        my $rel_row = $db->select(
            'releases',
            ['id', 'datetime', 'distro_id'],
            {
                # distro_id => $distro_id,
                author    => $author,
                name      => $name,
                # datetime  => pg_dt($dt),
            },
        )->hash;
        if ($rel_row) {
            pg_dt($rel_row->{datetime}) == $dt or die "different datetime: ($rel_row->{id}) $rel_row->{datetime} != $dt";
            $rel_row->{distro_id} == $distro_id or die "different distro_id: ($rel_row->{id}) $rel_row->{distro_id} != $distro_id";

            next RELEASE;
        } else {
            $rel_row = $db->insert(
                'releases',
                {
                    distro_id => $distro_id,
                    version   => $version,
                    datetime  => pg_dt($dt),
                    name      => $name,
                    author    => $author,
                },
                {
                    on_conflict => [
                        [ qw/author name/ ] => { name => \'releases.name' },
                    ],
                    returning   => [qw/ id datetime distro_id /],
                },
            )->hash;
            pg_dt($rel_row->{datetime}) == $dt
                and $rel_row->{distro_id} == $distro_id
                or die "different datetime or distro_id: $rel_row->{datetime} $dt $rel_row->{distro_id} $distro_id";
        }
        my $rel_id = $rel_row->{id};

        # 6. insert release_es
        $db->insert(
            'release_es',
            {
                rel_id => $rel_id,
                data   => to_json($rel),
            },
            { on_conflict => undef },
        );

        # 7. write changes
        my $url = "https://fastapi.metacpan.org/v1/changes/$author/$name";
        my ($path, $content);
        my $success;
        ATTEMPT:
        for (1..5) {
            try {
                my $res = ua->get($url)->result;
                if ($res->is_success) {
                    ($content, $path) = $res->json->@{qw/ content path /};
                    defined $content or die 'no content in success';
                    defined $path or die 'no path in success';
                } elsif ($res->code == 404) {
                    $content = undef;
                    $path = undef;
                } else {
                    die 'neither success nor 404';
                }

                $success = 1;

                last ATTEMPT;
            } catch ($e) {
                $success = 0;
                next ATTEMPT;
            };
        }

        $success or die "Couldn't download changes";

        $db->insert(
            'changes',
            {
                rel_id  => $rel_id,
                path    => $path,
                content => $content,
            },
        );

        ### DIFFS ###

        # 8. Reset processed flag of all future releases
        my $subsequent_releases = $db->select(
            'releases',
            'id',
            {
                distro_id => $distro_id,
                datetime  => { '>', pg_dt($dt) },
            },
        )->hashes;
        my @subsequent_ids = $subsequent_releases->map(sub {$_->{id}})->@*;
        $db->update(
            'changes',
            {
                diff         => undef,
                prev_id      => undef,
                is_processed => 0,
            },
            { rel_id => { -in, \@subsequent_ids } },
        );

        # 9. Discover diffs and update changes of the entire distro_id
        say "Updating changes";
        {
            my sub is_valid_version ($version_str) {
                my $produced_warning = 0;
                local $SIG{__WARN__} = sub ($msg) {
                    $produced_warning = 1;
                    warn $msg;
                };
                return is_lax($version_str) && eval { version->parse($version_str); 1 } && ! $produced_warning;
            }

            # get all distro's releases
            my @distro_rels = $db->select(
                [
                    'releases',
                    ['changes', rel_id => 'releases.id'],
                ],
                [
                    (map "releases.$_", qw/ id version datetime /),
                    (map "changes.$_", qw/ content is_processed /),
                ],
                {
                    distro_id => $distro_id,
                    content   => { '!=', undef },
                },
                { order_by => ['datetime'] },
            )->hashes->@*;

            # grep releases for valid versions and sort them by date
            @distro_rels = grep { is_valid_version($_->{version}) } @distro_rels;

            # turn version strings into version objects
            $_->{version} = version->parse($_->{version}) foreach @distro_rels;

            DISTRO_RELEASE:
            foreach my ($i, $d_rel) (indexed @distro_rels) {
                ! $d_rel->{is_processed} or next DISTRO_RELEASE;
                my $prev_rel = do {
                    my @smaller = grep {$_->{version} < $d_rel->{version}} @distro_rels[0 .. $i-1];
                    max_by {$_->{version}} reverse @smaller; # largest of small
                };
                # prev_rel might be undef
                my $prev_id = $prev_rel ? $prev_rel->{id} : undef;
                my $prev_content = $prev_rel ? $prev_rel->{content} : '';
                $db->update(
                    'changes',
                    {
                        prev_id      => $prev_id,
                        diff         => { -json => PLib::Diff->diff($prev_content, $d_rel->{content}) },
                        is_processed => 1,
                    },
                    { rel_id => $d_rel->{id} },
                );
            }
        }

        # 9. write provides
        say "Writing provides";
        {
            my $names_provided = $db->query(
                q|SELECT "data"->'provides' FROM "release_es" WHERE rel_id = ?|,
                $rel_id,
            )->expand->arrays->[0][0];

            NEW_MODULE_NAME:
            foreach my $new_name (uniq @$names_provided) {
                if (! exists $existing_modules_by_name->{$new_name}) {
                    my $mod_id = $db->insert(
                        'modules',
                        {
                            name      => $new_name,
                            distro_id => $distro_id,
                        },
                        { returning => 'id' },
                    )->hashes->[0]{id};

                    $existing_modules_by_name->{$new_name} = {
                        id        => $mod_id,
                        name      => $new_name,
                        distro_id => $distro_id,
                    };
                } else {
                    my $existing_distro_id = $existing_modules_by_name->{$new_name}{distro_id};
                    if (eqq($existing_distro_id, $distro_id)) {
                        next NEW_MODULE_NAME;
                    } else {
                        $db->update(
                            'modules',
                            { distro_id => $distro_id },
                            { id => $existing_modules_by_name->{$new_name}{id} },
                        );

                        $existing_modules_by_name->{$new_name}{distro_id} = $distro_id;
                    }
                }
            }
        }

        # 10. write dependencies
        say 'Writing dependencies...';
        {
            my $deps = $db->select(
                'release_es',
                [ \"release_es.data->'dependency' AS deps" ],
                { rel_id => $rel_id },
            )->expand->hashes->[0]{deps};

            foreach my $dep (sort_by {"$_->{module} $_->{phase} $_->{relationship}"} @$deps) {
                my $mod_name = $dep->{module} or die 'missing module';
                my $phase = $dep->{phase} or die 'missing phase';
                my $relationship = $dep->{relationship} or die 'missing relationship';

                my $mod_row = $db->select(
                    'modules',
                    'id',
                    { name => $mod_name },
                )->hashes->[0] || do {
                    my $row = $db->insert(
                        'modules',
                        { name => $mod_name },
                        {
                            on_conflict => [ 'name' => { name => \'modules.name' } ],
                            returning   => 'id',
                        },
                    )->hashes->[0];
                    $existing_modules_by_name->{$mod_name} = {
                        id        => $row->{id},
                        name      => $mod_name,
                        distro_id => undef,
                    };
                    $row;
                };
                my $mod_id = $mod_row->{id};

                eval {
                    $db->insert(
                        'deps',
                        {
                            distro_id    => $distro_id,
                            module_id    => $mod_id,
                            phase        => $phase,
                            relationship => $relationship,
                        },
                        { on_conflict => undef },
                    );
                };
            }
        }

        # 11. commit transaction
        $tx->commit;
    }

    $app->redis->delete('front_page') if @rels;

    sleep 0.5 * 3_600;
}
