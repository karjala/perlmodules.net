package Local::MetacpanAPI;

use v5.36;

use Mojo::Base -strict, -signatures;

use Local::Util 'es_dt';

use MetaCPAN::Client;

sub _mcpan {
    state $mcpan = do {
        my $_mcpan = MetaCPAN::Client->new;
        $_mcpan->ua->agent('perlmodules.net v0.9');
        $_mcpan;
    };
}

sub fetch_recent_releases ($class, $min_dt, $max_dt = undef) {
    # validate params
    $min_dt isa DateTime or die '$min_dt is not a DateTime';
    ! defined $max_dt or $max_dt isa DateTime or die '$max_dt (which is defined) is not a DateTime';

    # fetch data
    my $releases_iterator = _mcpan->all('releases', {
        es_filter => {
            and => [
                { term => { authorized => 1 } },
                { term => { maturity => 'released' } },
                {
                    range => {
                        date => {
                            gte => es_dt($min_dt),
                            defined $max_dt ? (lte => es_dt $max_dt) : (),
                        },
                    },
                },
            ],
        },
    });

    # form array from iterator
    my @releases;
    while (my $release = $releases_iterator->next) {
        push @releases, $release->{data};
    }

    return \@releases;
}

1;
