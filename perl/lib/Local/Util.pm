package Local::Util;

use v5.36;
use Mojo::Base -strict, -signatures;

use Mojo::UserAgent;
use DateTime::Format::Pg;
use DateTime::Format::RFC3339;
use DateTime::Format::ISO8601;
use DDP ();
use List::AllUtils 'any';
use Scalar::Util 'refaddr';

use Exporter 'import';
our @EXPORT_OK = qw/
    eqq belongs_to p pg_dt es_dt iso_dt hashify ua
/;

# like === of JavaScript, except for the fact that eqq(15, '15') returns true
sub eqq ($x, $y) {
    defined $x or return ! defined $y;
    defined $y or return !!0;
    ref $x eq ref $y or return !!0;
    length(ref $x) ? refaddr($x) == refaddr($y) : $x eq $y;
}

# like .includes(item) of JavaScript
sub belongs_to ($item, $arrayref) {
    return any { eqq $item, $_ } @$arrayref;
}

# converts:
#   - PostgreSQL timestamp strings to DateTime objects,
#   - DateTime objects and epoch numbers to PostgreSQL timestamp string,
sub pg_dt :prototype(_) ($thing) {
    my $number_re = qr/^[0-9]+(\.[0-9]+)?\z/;
    if (! length ref $thing and $thing =~ $number_re) {
        # epoch (number) to postgresql date string
        $thing = DateTime->from_epoch(epoch => $thing, time_zone => 'UTC') if $thing =~ $number_re;
        return DateTime::Format::Pg->format_timestamp_with_time_zone($thing);
    } elsif (! length ref $thing) {
        # postgresql date string to DateTime object
        return DateTime::Format::Pg->parse_timestamp_with_time_zone($thing);
    } elsif ($thing isa DateTime) {
        # DateTime object to postgresql date string
        if ($thing->time_zone->name ne 'UTC') {
            $thing = $thing->clone;
            $thing->set_time_zone('UTC');
        }
        return DateTime::Format::Pg->format_timestamp_with_time_zone($thing);
    } else {
        die 'invalid $thing: ', ref $thing;
    }
}

# converts:
#   - ES date strings to DateTime objects,
#   - DateTime objects and epoch numbers to ES date strings,
sub es_dt :prototype(_) ($thing) {
    my $number_re = qr/^[0-9]+(\.[0-9]+)?\z/;
    if (! length ref $thing and $thing =~ $number_re) {
        # epoch (number) to ES date string
        $thing = DateTime->from_epoch(epoch => $thing, time_zone => 'UTC');
        return DateTime::Format::RFC3339->format_datetime($thing);
    } elsif (! length ref $thing) {
        # ES date string to DateTime object
        return DateTime::Format::RFC3339->parse_datetime($thing);
    } elsif ($thing isa DateTime) {
        # DateTime object to ES date string
        if ($thing->time_zone->name ne 'UTC') {
            $thing = $thing->clone;
            $thing->set_time_zone('UTC');
        }
        return DateTime::Format::RFC3339->format_datetime($thing);
    } else {
        die 'invalid $thing: ', $thing;
    }
}

sub iso_dt :prototype(_) ($thing) {
    my $number_re = qr/^[0-9]+(\.[0-9]+)?\z/;
    if (! length ref $thing and $thing =~ $number_re) {
        # epoch (number) to ISO date string
        $thing = DateTime->from_epoch(epoch => $thing, time_zone => 'UTC');
        return DateTime::Format::ISO8601->format_datetime($thing);
    } elsif (! length ref $thing) {
        # ISO date string to DateTime object
        return DateTime::Format::ISO8601->parse_datetime($thing);
    } elsif ($thing isa DateTime) {
        # DateTime object to ISO date string
        if ($thing->time_zone->name ne 'UTC') {
            $thing = $thing->clone;
            $thing->set_time_zone('UTC');
        }
        return DateTime::Format::ISO8601->format_datetime($thing);
    } else {
        die 'invalid $thing: ', $thing;
    }
}

# dumps nicely, with colors
sub p :prototype(_) ($thing) {
    DDP::p($thing);
}

# turns arrays of hashes into hash of hashes by $fields
sub hashify ($hashes, $fields, $sub = undef) {
    my $ret = {};

    foreach my $hash (@$hashes) {
        my $cursor = \$ret;
        foreach my $field (@$fields) {
            my $value = $hash->{$field};
            $cursor = \($$cursor->{$value} //= {});
        }
        $$cursor = $sub ? do {
            local $_ = $hash;
            $sub->($hash);
        } : $hash;
    }

    return $ret;
}

sub ua () {
    state $ua = do {
        my $_ua = Mojo::UserAgent->new;
        $_ua->transactor->name('perlmodules 0.9');
        $_ua->request_timeout(30);
        $_ua->connect_timeout(20);
        $_ua;
    };
}

1;
