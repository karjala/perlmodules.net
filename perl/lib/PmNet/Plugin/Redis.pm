package PmNet::Plugin::Redis;

use v5.36;
use Mojo::Base 'Mojolicious::Plugin';

use Mojo::Redis;
use Mojo::JSON 'from_json', 'to_json';

sub register ($self, $app, $config) {
    my $redis = Mojo::Redis->new;
    $app->helper('redis.get' => sub ($c, $key) {
        return from_json $redis->db->get($key);
    });
    $app->helper('redis.set' => sub ($c, $key, $value) {
        return $redis->db->set($key => to_json $value);
    });
    $app->helper('redis.get_or_set' => sub ($c, $key, $sub) {
        my $value = $c->redis->get($key);
        if (! defined $value) {
            $value = $sub->();
            $c->redis->set($key, $value);
        }
        return $value;
    });
    $app->helper('redis.delete' => sub ($c, $key) {
        $redis->db->del($key);
    })
}

1;
