package PmNet::Plugin::Various;

use v5.36;
use Mojo::Base 'Mojolicious::Plugin';

use Mojo::Pg;

sub register ($self, $app, $config) {
    $app->helper(pg => sub ($c) {
        state $pg = Mojo::Pg->new('postgresql://dbuser:dbuser@localhost/pmnet');
    });
}

1;
