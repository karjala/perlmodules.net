package PmNet::Controller::API;

use v5.36;
use Mojo::Base 'Mojolicious::Controller';

use PLib::Diff;
use Local::Util 'pg_dt';

sub get_all_releases ($self) {
    my $db = $self->pg->db;

    my $releases = $db->select(
        [
            'releases',
            ['distros', id => 'releases.distro_id'],
        ],
        [qw/ releases.id distros.name releases.version datetime /],
        undef,
        {
            order_by => { -desc, 'datetime' },
            limit    => 10,
        },
    )->hashes;

    $self->render(json => [
        map +{
            id        => int $_->{id},
            name      => $_->{name},
            'version' => $_->{version},
            epoch     => pg_dt $_->{datetime},
        }, @$releases
    ]);
}

sub get_distro_feed ($self) {
    # params
    my $distro_name = $self->param('distro_name');

    my $db = $self->pg->db;

    my $releases = $db->select(
        [
            'releases',
            ['distros', id => 'releases.distro_id'],
            ['changes', rel_id => 'releases.id'],
        ],
        [qw/ releases.name changes.diff releases.datetime /],
        { 'distros.name' => $distro_name },
        {
            order_by => { -desc, 'releases.datetime' },
            limit    => 15,
        },
    )->expand->hashes;

    $self->render(json => [
        map +{
            name  => $_->{name},
            diff  => PLib::Diff->draw_diff($_->{diff}),
            epoch => pg_dt $_->{datetime},
        }, @$releases
    ]);
}

1;
