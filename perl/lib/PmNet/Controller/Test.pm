package PmNet::Controller::Test;

use Mojo::Base 'Mojolicious::Controller', -signatures;

sub hello_world ($self) {
    $self->render(text => 'hello world!', format => 'txt');
}

1;
