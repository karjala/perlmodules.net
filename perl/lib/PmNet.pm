package PmNet;
use Mojo::Base 'Mojolicious', -signatures;

use PLib::Init;

use Syntax::Keyword::Try;

use experimental 'isa';

# This method will run once at server start
sub startup ($self) {

    # Load configuration from config file
    push $self->plugins->namespaces->@*, 'PmNet::Plugin';
    my $config = $self->plugin('Config');
    $self->plugin('Various');
    $self->plugin('Redis');

    # Configure the application
    $self->secrets($config->{secrets});

    # Router
    my $r = $self->routes;

    # Normal route to controller
    my $api_r = $r->any('/api')->to('API#');
    $api_r->get('/hello-world')->to('test#hello_world');
    $api_r->get('/releases')->to('#get_all_releases');
    $api_r->get('/distro/:distro_name/feed')->to('#get_distro_feed');
}

1;

