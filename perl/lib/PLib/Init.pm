package PLib::Init;

use Mojo::Base -strict, -signatures;

use Local::Util;

use Mojo::Pg::Results;
use Mojo::Pg::Database;
use DateTime;

sub Mojo::Pg::Results::hashify {
    my $hashes = shift->hashes;

    return Local::Util::hashify($hashes, @_);
}

# check whether a row exists in $table conforming to $where
sub Mojo::Pg::Database::exists ($db, $table, $where = undef) {
    my ($sql, @bind) = $db->pg->abstract->select($table, undef, $where);
    $sql = "SELECT EXISTS ( $sql )";
    return $db->query($sql, @bind)->array->[0];
}

sub DateTime::TO_JSON { int $_[0]->epoch }

1;
