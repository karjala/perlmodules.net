package PLib::Diff;

use v5.34;
use warnings;

use Algorithm::Diff;
use Mojo::Util qw( xml_escape );

sub diff {
    my ($class, $text1, $text2) = @_;

    my @text1 = $text1 =~ /^(.*?)$/mg;
    my @text2 = $text2 =~ /^(.*?)$/mg;

    undef @text1 if not length $text1;
    undef @text2 if not length $text2;

    my $diff = Algorithm::Diff->new(\@text1, \@text2);

    $diff->Base(1);
    my $buffer;
    my $virgin = 1;
    my @struct;
    while ($diff->Next) {
        if ($diff->Same) {
            if ($virgin) { next; }
            $buffer = ['same', scalar @{[ $diff->Items(1) ]}];
        } else {
            $virgin = 0;
            push @struct, $buffer	if $buffer;
            undef $buffer;
            push @struct, ['changes'];
            push $struct[-1]->@*, ($diff->Items($_) ? [$diff->Items($_)] : undef)	foreach (1, 2);
        }
    }

    return \@struct;
}

sub draw_diff {
    my ($class, $struct) = @_;

    my $ret = '<pre>';
    foreach my $entry (@$struct) {
        if ($entry->[0] eq 'same') {
            $ret .= '<br /><div style="color: grey;">['.$entry->[1].' same line';
            $ret .= ($entry->[1] > 1 ? 's' : '').']</div><br />';
        } elsif ($entry->[0] eq 'changes') {
            my @styles = (
                undef,
                'color: red; text-decoration: line-through;',
                'color: green;'
            );
            foreach my $i (1, 2) {
                if ($entry->[$i]) {
                    $ret .= qq{<div style="$styles[$i]">};
                    $ret .= join("<br />", map xml_escape($_), $entry->[$i]->@*);
                    $ret .= qq{</div>};
                }
            }
        }
        $ret .= "";
    }
    $ret .= '</pre>';

    return $ret;
}

1;

