-- CREATE USER "dbuser" WITH PASSWORD 'dbuser';
-- CREATE DATABASE "pmnet" WITH OWNER "dbuser";


CREATE TABLE "distros" (
    "id"                BIGSERIAL               NOT NULL,
    "name"              TEXT                    NOT NULL    CHECK(LENGTH("name") < 2048),

    PRIMARY KEY ("id")
);
CREATE UNIQUE INDEX ON "distros" ("name");


CREATE TABLE "releases" (
    "id"                BIGSERIAL               NOT NULL,
    "distro_id"         BIGINT                  NOT NULL    REFERENCES "distros",
    "version"           TEXT                    NOT NULL    CHECK(LENGTH("version") < 2048),
    "datetime"          TIMESTAMPTZ             NOT NULL,
    "name"              TEXT                    NOT NULL    CHECK(LENGTH("name") < 2048),
    "author"            TEXT                    NOT NULL    CHECK(LENGTH("author") < 2048),

    PRIMARY KEY ("id")
);
CREATE UNIQUE INDEX ON "releases" ("author", "name");
CREATE INDEX ON "releases" ("distro_id", "datetime");
CREATE INDEX ON "releases" ("datetime");


CREATE TABLE "release_es" (
    "rel_id"            BIGSERIAL               NOT NULL    REFERENCES "releases",
    "data"              JSONB                   NOT NULL,

    PRIMARY KEY ("rel_id")
);
CREATE INDEX ON "release_es" USING GIN ("data");


-- releases that aren't/couldn't be stored in "releases" table
CREATE TABLE "buggy_es" (
    "id"                BIGSERIAL               NOT NULL,
    "author"            TEXT                        NULL,
    "rel_name"          TEXT                        NULL,
    "data"              JSONB                   NOT NULL,

    PRIMARY KEY ("id")
);
CREATE INDEX ON "buggy_es" ("author", "rel_name");
CREATE INDEX ON "buggy_es" USING GIN ("data");


CREATE TABLE "modules" (
    "id"                BIGSERIAL               NOT NULL,
    "name"              TEXT                    NOT NULL    CHECK(LENGTH("name") < 2048),
    "distro_id"         BIGINT                      NULL,

    PRIMARY KEY ("id")
);
CREATE UNIQUE INDEX ON "modules" ("name");


CREATE TABLE "deps" (
    "distro_id"         BIGINT                  NOT NULL    REFERENCES "distros",
    "module_id"         BIGINT                  NOT NULL    REFERENCES "modules",
    "phase"             TEXT                    NOT NULL    CHECK(LENGTH("phase") < 100),
    "relationship"      TEXT                    NOT NULL    CHECK(LENGTH("relationship") < 100),

    PRIMARY KEY ("distro_id", "module_id", "phase", "relationship")
);


CREATE TABLE "changes" (
    "rel_id"            BIGINT                  NOT NULL    REFERENCES "releases",
    "path"              TEXT                        NULL,
    "content"           TEXT                        NULL,
    "diff"              JSONB                       NULL,
    "prev_id"           BIGINT                      NULL,
    "is_processed"      BOOLEAN                 NOT NULL    DEFAULT false,

    PRIMARY KEY ("rel_id")
);

-- CREATE TABLE "changes" (
--     "rel_id"            BIGINT                   NOT NULL    REFERENCES "release",
--     "path"              TEXT                    NOT NULL,
--     "file_contents"     TEXT                        NULL,
--
--     PRIMARY KEY ("rel_id")
-- );
